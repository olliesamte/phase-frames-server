var news = [
   "Borussia Dortmund wins German championship",
   "Tornado warning for the Bay Area",
   "More rain for the weekend",
   "Android tablets take over the world",
   "iPad2 sold out",
   "Nation's rappers down to last two samples"
];

var osc = require('osc-min');
var dgram = require('dgram'); 
var server = dgram.createSocket("udp4"); 
server.bind(8088, function() {
  server.setBroadcast(true);
  server.setMulticastTTL(128);
  server.setMulticastLoopback(true);
  server.addMembership("224.1.1.1");

  server.on('listening', function () {
    var address = server.address();
    console.log("server listening " + address.address + ":" + address.port);
  });

  server.on("message", function (msg, rinfo) {
    console.log("server got: " + msg + " from " + rinfo.address + ":" + rinfo.port);
  });
});

/*setInterval(broadcastNew, 3000);

function broadcastNew() {
    var message = new Buffer(news[Math.floor(Math.random()*news.length)]);
    server.send(message, 0, message.length, 8088, "224.1.1.1");
    console.log("Sent " + message + " to the wire...");
    //server.close();
}*/
var num = 0;
var sendHeartbeat = function() {
  var buf = osc.toBuffer({
    address: "/heartbeat",
    args: [
      12,
      "sttttring",
      new Buffer("beat"),
      { type: "integer", value: num }
    ]
  });
  num++;
  console.log("Sending OSC Message");
  server.send(buf, 0, buf.length, 8088, "224.1.1.1");
};

setInterval(sendHeartbeat, 2000);

