
var osc = require ('osc-min');
var udp = require ('dgram');

if (process.argv[2]){
    var inport = parseInt(process.argv[2]);
}
else{
    var inport = 41234;
}

console.log ("OSC listener running at http://localhost:" + inport);

//~verbatim:examples[0]~
//### A simple OSC printer`
var sock = udp.createSocket( "udp4", function(msg, rinfo){ 
    try {
        console.log(osc.fromBuffer(msg));
    }
    catch (error){
        console.log("invalid OSC packet");
    }
    });
sock.bind(inport); 