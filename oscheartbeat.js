var osc = require('osc-min');
var dgram = require('dgram'); 
var server = dgram.createSocket("udp4"); 

//server.setBroadcast(true)
//server.setMulticastTTL(128);
//server.setMulticastLoopback(true);
//server.addMembership("224.1.1.1");

var num = 0;
var sendHeartbeat = function() { 
  var buf = osc.toBuffer({
    address: "/heartbeat",
    args: [
      12,
      "sttttring",
      new Buffer("beat"),
      { type: "integer", value: num }
    ]
  });
  num++;
  console.log("Sending OSC Message");
  server.send(buf, 0, buf.length, 41234, "224.1.1.1");
};

setInterval(sendHeartbeat, 2000);