var dgram = require('dgram'); 
var osc = require ('osc-min');
var server = dgram.createSocket("udp4"); 
server.bind(8088, function() {
  server.addMembership("224.1.1.1");

  server.on('listening', function () {
    var address = server.address();
    console.log("server listening " + address.address + ":" + address.port);
  });

  server.on("message", function (msg, rinfo) {
    console.log("server got: " + msg + " from " + rinfo.address + ":" + rinfo.port);
  	console.log(osc.fromBuffer(msg));
  });
});